! Example, fortran version
program main
  implicit none
  integer, parameter :: linebuf = 1024

  ! User-defined parameters
  integer, parameter :: dim = 5
  real(8), parameter :: initial(dim) = (/ 10.0, 10.0, 5.0, 10.0, 5.0 /)
  real(8), parameter :: deviation(dim) = (/ 5.0, 0.5, 1.0, 1.0, 1.0 /)
  real(8), parameter :: error_tolerance = 1.0d-4
  real(8), parameter :: noise_level = 0.0d0
  character(len=*), parameter :: funname = "evalfun"

  character(len=linebuf) :: line, temp
  character(len=13) :: fmt
  real(8), allocatable :: input_values(:, :), output_values(:)
  integer :: i, j, n

  do
     read (*, "(A)") line
     write (0, *) ">", trim(line)
     if(trim(line) == "parameter")then
        write (6, "(A,A,I5)", advance="NO") "parameter ", funname, dim
        write (0, "(A)") "(Recieved parameter command)"
        write (0, "(A,A,I5)", advance="NO") "< parameter ", funname, dim
        if(noise_level == 0.0d0)then
           write (6, "(A)", advance="NO") " exact"
           write (0, "(A)", advance="NO") " exact"
        else
           write (6, "(A,E24.15)", advance="NO") " ", noise_level
           write (0, "(A,E24.15)", advance="NO") " ", noise_level
        end if
        write (6, "(E24.15)", advance="NO") error_tolerance
        write (0, "(E24.15)", advance="NO") error_tolerance
        do i = 1, dim
           write (6, "(E24.15,E24.15)", advance="NO") initial(i), deviation(i)
           write (0, "(E24.15,E24.15)", advance="NO") initial(i), deviation(i)
        end do
        write (6, "()", advance="YES")
        write (0, "()", advance="YES")
     elseif(trim(line) == "exit")then
        write (0, "(A)") "(Recieved exit command)"
        exit
     elseif(line(1:8) == "evaluate") then
        write (0, "(A)", advance="NO") "(Recieved evaluate command with n = "
        read (line, *) temp, n
        write (0, "(I3,A)") n, ")"
        allocate(input_values(dim, n))
        allocate(output_values(n))
        do i = 1, n
           read (*, *) (input_values(j, i), j = 1, dim)
        end do
        
        ! Evaluate function with evalfun
        ! You can also evaluate function values in parallel
        do i = 1, n
           output_values(i) = evalfun(input_values(:, i))
        end do

        write (fmt, "(I3)") n
        fmt = "(A," // trim(fmt) // "E24.15)"
        write (6, fmt) "evaluate", output_values(1:n)
        write (0, fmt) "<evaluate", output_values(1:n)
        deallocate(input_values, output_values)
     end if
  end do

contains
  real(8) function evalfun(v)
    real(8), intent(in) :: v(dim)
    integer :: i
    evalfun = 0.0d0
    do i = 1, dim
       evalfun = evalfun + floor(v(i) + 0.5) ** 2
    end do
  end function evalfun
end program main

